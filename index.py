
from flask import Flask, request, Response, abort, jsonify
from werkzeug.utils import secure_filename ##lib for secure filename
from geopy.distance import geodesic  ##lib for calculate distance between points

import os, csv, json, requests ## csv lib for work with csv files

UPLOAD_FOLDER = '/var/www/py_task/uploads'
##print(UPLOAD_FOLDER)
ALLOWED_EXTENSIONS = {'csv'}
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER ##directory for upload file

@app.route("/getaddresses", methods=['POST'])
def getaddresses():
    file = request.files['filedata']
    filename = secure_filename(file.filename)
    if file :
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename)) ##save file to /uploads dir
        if 'filedata' not in request.files:
            return {'status': 'error', 'messege': 'no file in request'}

        if file.filename == '':
            return {'status': 'error', 'messege': 'No selected file'}
    coordinats = []
    with open(UPLOAD_FOLDER +'/' + filename) as File:
        reader = csv.reader(File, delimiter=';') # convert csv file to obj
        # print(reader)


        for row in reader:
            coordinats.append(row)

    coordinats.pop(0) ## delete first string off

    points =[]
    for address in coordinats:  ## send request to get place name by coordinats
        place = requests.get('http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?location=' + address[1]+'%2C' + address[2] + '&langCode=en&outSR=&forStorage=false&f=json')
        place = place.json()

        if 'error' in place:   
            js = {'error':'Try other coordinats', 'c1':address[1], 'c2':address[2]}
            return Response(json.dumps(js),  mimetype='application/json')
        else:
       # print(place['address']['PlaceName'])
            points.append({'name':address[0], 'address': place['address']['PlaceName']})       # print(place['address']['PlaceName'])

    links_no_sort = {}
    tempcoordinats = coordinats
    tempcoordinats2 = coordinats


    for cords1 in tempcoordinats: ## creating list of distances between all points
        for cords2 in tempcoordinats2:
            between = cords1[0] + cords2[0]

            coordinat1 = (cords1[1], cords1[2])
            coordinat2 = (cords2[1], cords2[2])
            points_distance = geodesic(coordinat1, coordinat2).km
            if points_distance > 0:
                links_no_sort.update({ between: points_distance})

    result_sort_links = {} ## sorting from same value
    for key, value in links_no_sort.items():
        if value not in result_sort_links.values():
            result_sort_links[key] = value

    unic_lincs_distance = [] ## put values in list of dict
    for key, value in result_sort_links.items():
        unic_lincs_distance.append({'name':key, 'distance': value})

    js = {'points':points, "links": unic_lincs_distance}
    return Response(json.dumps(js),  mimetype='application/json')


@app.route("/")
def home():
	return 'Толян чо каво'

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return '<div align="center"><iframe width="560" height="315" align="middle" src="https://www.youtube.com/embed/5Nj2BngIko0?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>', 404



if __name__ == '__main__':
    app.run()


