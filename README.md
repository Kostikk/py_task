## Test task Python, Flask
Implement a simple rest API using Python Flask framework.
API should be able run as a stand-alone service on Linux machines and be accessible by
command-line tools ( cURL, wget, e.t.c ).
The API should expose the following method:
1. “getAddresses” HTTP POST API. This is a HTTP POST method that enables to
upload a CSV file with point locations ( LAT/LON ) having following structure ( 3
columns ) and unlimited number of rows:

|Point |Latitude |Longitude |
|:----:|:-------:|---------:|
|A     |50.448069|30.5194453|
|B     |50.448616|30.5116673|
|C     |50.913788|34.7828343|

getAddresses method should respond with JSON containing readable addresses ( or
place names ) for each point location and all possible direct links between each point
including distance in meters. The number of points should be unlimited.
See below JSON response format:

    { 
        “points”: 
        [
            { “name” : “A”, “address” : “Some address…” },
	        { “name” : “B”, “address” : “Some address…” },
	        { “name” : “C”, “address” : “Some address…” },
        ],
        “links”
        [
            { “name” : “AB”, “distance” : 350.6 },
            { “name” : “BC”, “distance” : 125.8 },
            { “name” : “AC”, “distance” : 1024.9 }
        ]
    }

Addresses can be generated with ArcGIS Online Geocoding Service, see below
example:

http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?location=30.5194453%2C50.448069&langCode=en&outSR=&forStorage=false&f=pjson

2. The source code should be delivered through a github project
3. Add README file to the project with detailed instructions on how to build and run it
locally ( using wGET or cURL command line tools ).
4. No need to deploy to the cloud.

## Installing

1. 
    ```bash
    git clone
    ```
2. 
    ```bash
    pip3 install geopy, flask, requests, werkzeug
    ```
  
3.  
    ```
    python3 server.py
    ```

4. 
    Windows CMD
    ```
    curl -i -X post -F filedata=@C:/Users/user/Desktop/test/2.csv http://127.0.0.1:5000/getaddresses
    ```


    Linux term
    ```
    curl -i -X post -F filedata=@home/proj/test/2.csv http://127.0.0.1:5000/getaddresses
    ```



    ```
    curl -i -X post -F filedata=@path/to/directory http://127.0.0.1:5000/getaddresses
    ```

5. enjoy
